Source: ruby-graffiti
Section: ruby
Priority: optional
Maintainer: Debian Ruby Extras Maintainers <pkg-ruby-extras-maintainers@lists.alioth.debian.org>
Uploaders: Dmitry Borodaenko <angdraug@debian.org>
Build-Depends: debhelper (>= 9~),
               gem2deb,
               ruby-sequel,
               ruby-sqlite3,
               syncache
Standards-Version: 3.9.7
Vcs-Git: https://anonscm.debian.org/git/pkg-ruby-extras/ruby-graffiti.git
Vcs-Browser: https://anonscm.debian.org/cgit/pkg-ruby-extras/ruby-graffiti.git
Homepage: http://samizdat.nongnu.org/
XS-Ruby-Versions: all

Package: ruby-graffiti
Architecture: all
XB-Ruby-Versions: ${ruby:Versions}
Depends: ruby | ruby-interpreter,
         ruby-pg | ruby-mysql2 | ruby-sqlite3,
         ruby-sequel,
         syncache,
         ${misc:Depends},
         ${shlibs:Depends}
Suggests: ruby-sequel-pg
Description: Relational RDF store for Ruby
 Graffiti is an RDF store based on dynamic translation of RDF queries into SQL.
 Graffiti allows one to map any relational database schema into RDF semantics
 and vice versa, to store any RDF data in a relational database.
 .
 Graffiti uses Sequel to connect to database backend and provides a DBI-like
 interface to run RDF queries in Squish query language from Ruby applications.
